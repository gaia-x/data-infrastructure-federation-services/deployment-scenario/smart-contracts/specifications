
# Smart Contract Manager


## Sammary



- [Smart Contract Manager](#smart-contract-manager)
  - [Introduction](#introduction)
  - [Scheme Workflow](#scheme-workflow)
  - INTEGRER A L INTRO[Transparency and Uniformity with ODRL](#transparency-and-uniformity-with-odrl)
- [General Information](#general-information)
  - [Usage](#usage)
  - [Why Smart Contracts?](#why-smart-contracts)
  - [Flexible Terms and Plugins](#flexible-terms-and-plugins)
- [Description of the Deployment Scenario](#description-of-the-deployment-scenario)
  - [Define Contracts with ODRL](#define-contracts-with-odrl)
  - [Smart Contract Manager](#smart-contract-manager-2)
- [Architecture](#architecture)
  - [Overview of Components](#overview-of-components)
  - [The Smart Contract Manager](#the-smart-contract-manager)
  - [The Oracle](#the-oracle)
  - [The Payment Gateway](#the-payment-gateway)
  - [Detailed Component Descriptions](#detailed-component-descriptions)
    - [Smart Contracts on the Blockchain](#smart-contracts-on-the-blockchain)
    - [Deployment of Smart Contracts](#deployment-of-smart-contracts)
    - [Modular vs. Monolithic Architectures](#modular-vs-monolithic-architectures)
    - [Smart Contract Registry](#smart-contract-registry)
    - [Smart Contract Consumption](#smart-contract-consumption)
    - [Billing Contract](#billing-contract)
    - [Payment Contract](#payment-contract)
  - [Deploy Smart Contracts](#deploy-smart-contracts)
  - [Front Consumption: Usage, Example, and Integration](#front-consumption-usage-example-and-integration)
  - [Oracle Integration](#oracle-integration)
  - [Cron Jobs (if necessary)](#cron-jobs-if-necessary)
  - [Gateway](#gateway)
  - [Results](#results)
- [Entry Point](#entry-point)
- [Routing](#routing)
  - [Smart Contract Manager](#smart-contract-manager-3)
  - [Oracle](#oracle-2)
    - [Payment](#payment)
    - [Billing](#billing)
- [Steps to Install the Project Locally](#steps-to-install-the-project-locally)
- [Add a Plugin](#add-a-plugin)
- [Technologies and Tools Used](#technologies-and-tools-used)
- [Authors and Acknowledgment](#authors-and-acknowledgment)
- [License](#license)


## Introduction <a name="introduction"></a>
The Smart Contract Manager is designed to enhance trust and transparency in the management of contracts between service providers and consumers. It achieves this by enabling precise tracking of contract terms and automating related procedures. This system leverages smart contracts to ensure that all parties adhere to agreed-upon conditions, thereby reducing the potential for disputes and ensuring a seamless contract execution process.


The Smart Contract Manager defines the process for users to create and deploy smart contracts efficiently. By utilizing ODRL for policy definitions and integrating real-time data through oracles, this project aims to formalize and enhance the contractualization of digital agreements such as payment or billing.



Thanks to this framework, we have established a comprehensive system enabling providers to create, manage, and deploy smart contracts with ease. This process allows developers to connect to the system using their chosen development environments to build and extend smart contracts. The Smart Contract Manager acts as an intermediary step between defining contract terms in ODRL and deploying these contracts to a blockchain.


### Scheme workflow


![Composant_diagram](image/architecture1.jpg)

## Transparency and Uniformity with ODRL <a name="ODRL"></a>



To address the need for transparency and readability, the blockchain technology and smart contracts were chosen; ODRL (Open Digital Rights Language) was selected for standardization and accessibility. By utilizing the capabilities of ODRL, we can automate specific procedures related to the contract, ensuring strict adherence to the agreed terms. This reduces human errors, improves process efficiency, and ensures constant compliance with contract conditions.



For instance, recurring tasks such as billing, payments, and consumption tracking can be fully automated. Smart contracts, configured from ODRL, can verify and validate conditions in real-time, initiate specific actions when certain conditions are met, and record all transactions on the blockchain to ensure complete traceability and immutability.


## General information <a name="information"></a>



### Usage



The Smart Contract Manager is designed to simplify and enhance the process of creating, managing, and deploying smart contracts, particularly those based on ODRL (Open Digital Rights Language) contracts. This tool is essential for developers and organizations looking to streamline the contractualization of digital agreements. It is particularly useful in environments where dynamic and real-time data integration is crucial, such as in decentralized finance (DeFi) applications, supply chain management, and digital rights management. By leveraging plugins and integrating with oracles, the Smart Contract Manager ensures that contracts are not only defined clearly and accurately but also operate with the most current and relevant data. This makes it a valuable asset for scenarios that require automated, transparent, and enforceable agreements, such as token creation, secure voting systems, and the tracking and management of digital assets. Additionally, its modular and extensible architecture allows for customization and scalability, making it adaptable to various industries and use cases, including gaming, healthcare, and logistics. Overall, the Smart Contract Manager provides a comprehensive solution for deploying robust and reliable smart contracts, enhancing efficiency and trust in digital transactions.



### Why smart contract ?



Smart contracts are essential to the Smart Contract Manager, offering automation, transparency, and security. They streamline processes, ensure immutability, and reduce costs, making them ideal for DeFi, supply chain, and digital rights management. With blockchain security and real-time data from oracles, they provide accurate, transparent billing and payment solutions. The Smart Contract Manager uses these benefits to deliver a robust and efficient platform for managing smart contracts.



### Flexible terms and plugins



Our Smart Contract Manager features a versatile plugin system, allowing developers to easily extend and customize contract functionalities. This modular approach ensures that new capabilities can be integrated seamlessly. Additionally, ODRL terms can be easily modified or created, providing flexibility in defining and enforcing contract rules. This combination of plugins and customizable ODRL terms ensures a dynamic and adaptable platform for managing smart contracts.




## Description of the deployment scenario<a name="description"></a> Technical description



The deployment process takes place between defining the contract using ODRL and deploying the smart contract via the Smart Contract Manager. The aim is to ensure that smart contracts are created, integrated with oracles, and managed efficiently.



To implement the deployment process, we have set up a series of steps that formalize the creation and execution of smart contracts from ODRL definitions. This process is initiated at the Smart Contract Manager level and can be adapted for various blockchain environments.



When the deployment process begins, all the information concerning the ODRL contract is collected and integrated into the smart contract deployment process.



As far as oracle integration is concerned, it is up to each data provider to create its own oracle plugin. In our case, we have integrated proprietary oracle solutions to ensure real-time data updates.



The deployment process is as follows:



### Define Contracts with ODRL

The process begins with defining the smart contracts using ODRL. These definitions include all the necessary policy rules and conditions for the contract.



### Smart Contract Manager

Once the ODRL definitions are complete, they are fed into the Smart Contract Manager. The Smart Contract Manager interprets these definitions and prepares the smart contracts for deployment.

## Architecture <a name="architecture"></a>

## Overview of Components <a name="components-overview"></a>



The Smart Contract Manager operates in conjunction with a collection of decentralized applications (dApps), working together to achieve the goals mentioned above. The primary components include the Smart Contract Manager itself, the Oracle, and the Payment Gateway.



### The Smart Contract Manager



The Smart Contract Manager is responsible for generating and deploying smart contracts. It also initializes recurring tasks, such as payment procedures, to ensure the continuous monitoring and execution of contract terms. By using the Oracle, it ensures that contract-related tasks are scheduled and automated, maintaining efficiency and adherence to contract terms.



### The Oracle



The Oracle serves as an intermediary, bridging the smart contracts with other decentralized applications. It manages the initiation of recurring tasks like billing and payment procedures, ensuring compliance and efficiency in contract execution. The Oracle plays a crucial role in linking smart contracts with the Payment Gateway, facilitating seamless communication and data exchange.



### The Payment Gateway



The Payment Gateway is designed to facilitate the execution of payments through various payment solutions. It is easily extensible thanks to a plugin mechanism that allows for support of different payment solutions and methods. Currently, the Payment Gateway includes a plugin for Paynum, a payment solution developed by Docaposte, demonstrating its adaptability and extensibility.



## Detailed Component Descriptions <a name="detailed-description"></a>



### Smart Contracts on the Blockchain



Smart contracts are components deployed on a blockchain, allowing for automatic and secure transaction recording, ensuring total transparency and unalterable traceability of exchanges. The decentralized nature of blockchain eliminates the need for intermediaries, thus reducing costs and delays associated with traditional transactions.



Smart contracts are programmed to execute predefined actions when certain conditions are met, ensuring that all parties adhere to the terms of the agreement without the possibility of fraud or manipulation. Each transaction executed via a smart contract is timestamped and immutable, offering complete visibility and irrefutable proof of activities performed.



Additionally, smart contracts can interact with other smart contracts and decentralized applications, creating an automated and interconnected ecosystem that can handle complex and varied tasks. This capability for automation and interoperability makes smart contracts a powerful tool for modernizing and securing business, financial, and legal processes.



### Deployment of Smart Contracts



The deployment of the Smart Contract Manager is crucial for ensuring transparency and decentralization, fundamental principles of blockchain technology. Ideally, the management of the Smart Contract Manager should not be centralized under a single entity but rather handled by a decentralized federation.



A federation of stakeholders, including service providers, consumers, and regulators, could collaborate to manage the deployment and maintenance of the Smart Contract Manager. This federative structure would distribute decision-making power, reducing risks of centralization and conflicts of interest, ensuring the system remains neutral and fair for all involved parties.



For this purpose, decentralized governance mechanisms can be established, including voting processes for updates and modifications of smart contracts, and regular audits to ensure compliance and transparency of operations. This approach would build greater trust and wider adoption of smart contracts, while remaining aligned with the core values of blockchain.



### Modular vs. Monolithic Architectures



When designing an architecture for smart contracts, it's essential to consider the advantages and disadvantages of monolithic and modular approaches. A monolithic architecture is simpler to manage initially, with quicker internal communication and easier deployment, but it can become rigid and hard to evolve as requirements change.



To maximize flexibility and support future types of smart contracts, a modular architecture is preferable, deploying multiple smart contracts separately. This allows better adaptation to specific requirements, such as payment, billing, or consumption, and facilitates updating or replacing components without affecting others, improving system resilience and maintainability.



### Smart Contract Registry



In a complex system where multiple smart contracts must interact to meet the terms of a single contract (ODRL), an orchestration and management mechanism for these interactions becomes crucial. The smart contract registry plays an essential role in ensuring effective coordination and interaction among various deployed smart contracts on the blockchain.



### Smart Contract Consumption



When a service provider and a consumer establish a contract, it is crucial to ensure that the terms of this contract are respected in a transparent and verifiable manner. Managing consumption data is essential because any error or omission can lead to disputes, fraud, or violations of contract terms. The traceability of consumptions is therefore indispensable for ensuring a trustful relationship, enabling precise audits, and providing a solid basis for billing and payments.



To address this, we developed a consumption smart contract that takes as input consumption details or aggregates. This smart contract ensures clear and transparent management of consumption data, allowing all parties to view and verify this information in real-time, simplifying consumption tracking and ensuring greater trust in data accuracy.



Each consumption smart contract is linked to a single ODRL contract, storing information such as type, volume, and date of consumption. This integration allows verifying that recorded consumptions comply with the terms defined in the ODRL contract, offering complete transparency and transaction traceability.



Additionally, a web application was developed to provide files from external storage, generating a consumption record for each access or download. Developed in React with Keycloak for authentication and authorization management, users can connect via a wallet, ensuring secure authentication and access to the application’s functionalities and data. MinIO was used for storage, with each file download recorded in a consumption smart contract via an oracle, ensuring data transparency and integrity throughout the process.



### Billing Contract



Billing is a crucial component of any contractual agreement between a service provider and a consumer. Manual billing management can lead to errors, delays, and disputes, especially when consumptions vary and rates need to be applied precisely. Erroneous or non-transparent billing can lead to significant disagreements and undermine trust between the parties involved.



To automate the billing process while ensuring accuracy and transparency, a billing smart contract was developed. This smart contract takes a tariff table and consumptions as inputs, calculates the total bill and its details over a given period. This system ensures fully automated and transparent billing, allowing all parties to verify the details in real-time, fostering total trust in the billing process and eliminating error risks.



The billing smart contract contains a currency, a tariff table, and a list of generated bills according to the billing periodicity specified in the ODRL. An oracle initiates periodic calls to consumption sources, gathering necessary data and transmitting it to the billing smart contract, ensuring transparency and accuracy in billing.



### Payment Contract



Payment management involves three main challenges: payment transparency, free access to payment and billing data, and the integration of new payment methods or tools. The smart contract coupled with the oracle addresses transparency and accessibility issues, while the Payment Gateway facilitates the integration of multiple payment methods.



A payment smart contract was developed to manage individual or aggregated invoices over a period. This smart contract ensures automated and transparent payment management, simplifying transaction tracking and improving financial efficiency.



The Payment Gateway is equipped with a plugin system to integrate new payment solutions and methods easily. Currently, it includes a plugin for Paynum, a payment solution by Docaposte, demonstrating its capacity to adapt to additional payment methods as needed.



Designed for ease of integration, the Payment Gateway allows new payment solutions to be added simply by adding specific files for each new smart contract, defining required parameters and behaviors, reducing error risks and speeding up the deployment process. Integrated with Mailjet, it sends payment notifications containing the payment link, ensuring transparency and security throughout the payment process.



---



By working together, these components ensure that the Smart Contract Manager effectively enhances trust, transparency, and efficiency in the management and execution of contracts between service providers and consumers.



### Deploy Smart Contracts

The Smart Contract Manager then deploys the smart contracts to the chosen blockchain platform. This step involves compiling the contract code and ensuring it is correctly published on the blockchain.



### Front consumption: usage, exemple and integration



https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/smart-contracts/front-consumption

https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/smart-contracts/api-gateway-consumption

### Oracle Integration

After deployment, the smart contracts are connected to oracles. Oracles provide real-time data necessary for the execution and validation of the smart contracts. This ensures that the contracts operate based on the ost current information.



### Cron Jobs (if necessary)

For contracts requiring periodic updates or checks, cron jobs are set up. These cron jobs automate tasks such as data fetching from oracles or contract condition evaluations, ensuring that the smart contracts remain up-to-date and functional over time.



The tool we are making available can be integrated into various blockchain environments, subject to a few modifications.



### Gateway



https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/payment-gateway



### Results



## Entry point <a name="entrypoint"></a>

The Smart Contract Manager has an entry point which is an exposed route for the provider, consumer or other party to deploy the desired smart contracts on a blockchain.
The body request must be done following a unique template, as following :

```

{

"smartContractsToGenerate": [

"payment", "billing", "consumption"

],

"VC": {

"type": [

"VerifiableCredential",

"ContractCredential"

],

"@context": [

"https://schema-registry.aster-x.demo23.gxfs.fr/contexts/jws-2020",

"https://schema-registry.aster-x.demo23.gxfs.fr/contexts/credentials"

],

"id": "did:web:aster-x.demo23.gxfs.fr:d40fccec-ff23-4675-92df-eb04c9cb86ae:data.json",

"issuer": "did:web:aster-x.demo23.gxfs.fr",

"issuanceDate": "2023-12-20T16:11:56.656Z",

"validFrom": "2023-12-20T16:11:56.656Z",

"expirationDate": "2024-12-20T16:11:56.656Z",

"proof": {

"type": "JsonWebSignature2020",

"created": "2023-12-20T16:11:56.656Z",

"proofPurpose": "assertionMethod",

"verificationMethod": "did:web:aster-x.demo23.gxfs.fr",

"jws": "eyJhbGciOiJQUzI1NiIsImI2NCI6ZmFsc2UsImNyaXQiOlsiYjY0Il19..dLOswgi-NSviRFK4luCjPOil-00gLuF791gYa1TNnfu7wvsL-sokLmmgjtHCTqZTZb5pnrFneY0ScRfmoQbeDh_H7HBmATGIq0N7p30XnPPpU8wCpnKIl9VwXHoAH5Ie-tmTpHkwGJJENSr0ocOFFmshMbVihfsEvwExTrr6Y5wfoEU0GmkReRDSfRT7Zy5i8-4f9vE3UrvJCazCuilekY-83DPGE1IzS4Kpr9ZJP8Y6L7VK9A-_TZKn98qdHgVgZBdvwlO__IRh2NMkIk8Pqf0jEDgh2W-1mBA72xjsRgTjeOv43l1V1KWPsYPxGJsJH0JxEYeiJIFGyKA-wLvDrg"

},

"credentialSchema": {

"id": "https://registry.abc-federation.dev.gaiax.ovh/api/trusted-schemas-registry/v2/schemas/ContractCredential",

"type": "FullJsonSchemaValidator2021"

},

"credentialSubject": {

"id": "did:web:aster-x.demo23.gxfs.fr",

"gx-signature:participants": [

{

"gx-signature:did": "provider.agdatahub@proton.me",

"gx-signature:email": "yvan-wilfried.mbe-tene@softeam.fr"

},

{

"gx-signature:did": "jeremy.hirth-daumas@softeam.fr",

"gx-signature:email": "jeremy.hirth-daumas@softeam.fr"

}

],

"gx-signature:links": [

{

"gx-signature:documentId": 64817,

"gx-signature:documentName": "terms_and_conditions.pdf",

"gx-signature:documentOriginalName": "terms_and_conditions.pdf",

"gx-signature:documentLink": "https://www.contralia.fr/eDoc/user-api/document/view?token=UD2c969ea18c1a5dc3018c87fe2d202496-27937148950314067225"

},

{

"gx-signature:documentId": 49932,

"gx-signature:documentName": "ODRL.pdf",

"gx-signature:documentOriginalName": "ODRL.pdf",

"gx-signature:documentLink": "https://www.contralia.fr/eDoc/user-api/document/view?token=UD2c969ea18c1a5dc3018c87fe2d202496-27937148950314067225"

}

],

"gx:providedBy": "provider.agdatahub@proton.me",

"gx:consumedBy": "jeremy.hirth-daumas@softeam.fr",

"gx:termOfUsage": "https://example.com/terms-of-usage",

"gx:dataUsage": "https://example.com/data-usage",

"gx:notarizedIn": "https://example.com/notarizedIn",

"gx:dataProduct": {

"@context": [

"https://schema-registry.aster-x.demo23.gxfs.fr/contexts/credentials",

"https://schema-registry.aster-x.demo23.gxfs.fr/contexts/jws-2020"

],

"@type": [

"VerifiableCredential"

],

"@id": "did:web:agdatahub.provider.demo23.gxfs.fr:participant:eb06c24ac31a16b5f29316b388a742f2487ab626b5d22f23184ea7e8c3a3ed5b/vc/65b155f2810b649b9728a4eaf9239437b1bc34b1961998718080fafbcb3722a9/data.json",

"issuer": "did:web:agdatahub.provider.demo23.gxfs.fr",

"credentialSubject": {

"id": "did:web:agdatahub.provider.demo23.gxfs.fr:participant:eb06c24ac31a16b5f29316b388a742f2487ab626b5d22f23184ea7e8c3a3ed5b/vc/4203ae956f00530e5d68d157949dd3d0a6f07fcff21f3487e730785d53bc1e89/data.json",

"type": "gx:ServiceOffering",

"gx:providedBy": {

"id": "did:web:agdatahub.provider.demo23.gxfs.fr:participant:eb06c24ac31a16b5f29316b388a742f2487ab626b5d22f23184ea7e8c3a3ed5b/data.json"

},

"gx:termsAndConditions": {

"gx:URL": "https://icp-credentialissuer.aster-x.demo23.gxfs.fr/.well-known/TaureauxDeMontePublique",

"gx:hash": "c2c8bd85df0023cf7719def1a401b8fe076dacf5d61dc844080322d19ee309d9"

},

"gx:name": "Taureaux de Monte Publique : déclarations officielles",

"aster-conformity:layer": "IAAS",

"gx:keyword": [

"Data Product"

],

"gx:description": "Cette offre propose les données administratives des taureaux déclarés auprès de l’Institut de l’Elevage pour la monte publique. Elle contient les données d’identité, de parenté et de déclarations des taureaux avec leur période de validité, conformément à la réglementation. Le fichier est mis à jour hebdomadairement. Certains taureaux faisant l’objet déclarations complémentaires, un même taureau peut apparaître plusieurs fois dans le fichier.",

"gx:descriptionMarkDown": "",

"gx:webAddress": "",

"gx:dataProtectionRegime": [

"GDPR2016"

],

"gx:dataAccountExport": [

{

"gx:requestType": "email",

"gx:accessType": "digital",

"gx:formatType": "mime/png"

}

],

"gx:isAvailableOn": [

""

],

"gx:policy": "default: allow",

"@context": [

"https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#",

"https://schema-registry.aster-x.demo23.gxfs.fr/contexts/aster-conformity"

],

"gx-data:dataDomains": "",

"gx-data:dataController": "",

"gx-data:consent": "",

"gx:policies": "",

"gx-data:obsoleteDateTime": null,

"gx-data:expirationDateTime": null,

"gx:aggregationOf": [

{

"dct:identifier": "./dataSet/dataset01",

"dct:title": "dataset01",

"gx:description": "",

"gx:copyrightOwnedBy": [

"France Génétique Elevage"

],

"gx:personalDataPolicy": "",

"gx:expirationDateTime": null,

"gx:obsoleteDateTime": null,

"dct:distribution": [

{

"title": "2023100820231014-TaureauxMP",

"dct:format": "csv",

"dcat:byteSize": "5319062",

"gx:location": "https://platform.api-agro.eu/members/products/data-offerings/a01ed1fbd27245fa85bc9cec23a3e297/view",

"gx:hash": "",

"gx:hashAlgorithm": "None",

"gx:expirationDateTime": null,

"gx:obsoleteDateTime": null

}

]

}

]

},

"issuanceDate": "2023-11-07T12:20:57.161303+00:00",

"proof": {

"type": "JsonWebSignature2020",

"proofPurpose": "assertionMethod",

"verificationMethod": "did:web:agdatahub.provider.demo23.gxfs.fr",

"created": "2023-11-07T12:20:57.161303+00:00",

"jws": "eyJhbGciOiAiUFMyNTYiLCAiYjY0IjogZmFsc2UsICJjcml0IjogWyJiNjQiXX0..WCn13wS8pZMsYJcC2tXdZXn385PauLGRvHVLBXSl5Va_C7QP-6NNugswDMOoX36x-bkjkrn4zc4Itse4zu00EEZH9S5nfHbThfWYvBkKiLhk3A8Y6AWTievWeL-aC7FSqDTqn8yT17v8DCxm_HJyUyRaGCSHjKhgAxDDwRkXeiNqZYn1M1nR-5Uyakivve4U1Ffj6kNWjFEJ-k7DqoywZGQnGvAf-B1aoTSAsErJ9dNlKdPe4bFAE9yc0trjhBKToINdbwRE5TJ-UldAbjiHZfFhMJ8dFqF0zyym-L8X6exaRLNV6HaU0K_wTX6FH1HYPcb2QSwVmnYDHV_BSoxg0Q"

}

},

"odrl:hasPolicy": {

"@context": "https://www.w3.org/ns/odrl.jsonld",

"@type": "Set",

"uid": "https://agdatahub.provider.demo23.gxfs.fr/policy:1",

"nomService": "agdatahub",

"permission": [

{

"target": "https://minio.demo23.gxfs.fr/dataproduct/dataproduct01",

"assignee": "Data Subscriber",

"action": "use",

"duty": [

{

"assigner": "Data Provider Inc.",

"assignee": "Data Subscriber",

"action": [

{

"rdf:value": {

"@id": "odrl:compensate"

},

"refinement": [

{

"leftOperand": "payAmount",

"operator": "eq",

"rightOperand": {

"@value": "135.00",

"@type": "xsd:decimal"

},

"unit": "http://dbpedia.org/resource/Euro"

},

{

"leftOperand": "timeInterval",

"operator": "eq",

"rightOperand": "PT60S"

},

{

"leftOperand": "payMethod",

"operator": "eq",

"rightOperand": "creditCard"

},

{

"leftOperand": "paySolution",

"operator": "eq",

"rightOperand": "paynum"

},

{

"leftOperand": "creditorCode",

"operator": "eq",

"rightOperand": "gaia-x"

},

{

"leftOperand": "debitorEmail",

"operator": "eq",

"rightOperand": "moez.somai@softeam.fr"

}

]

},

{

"rdf:value": {

"@id": "odrl:billing"

},

"periodicity": {

"leftOperand": "timeInterval",

"operator": "eq",

"rightOperand": "PT20S"

},

"refinement": [

[

{

"leftOperand": "type",

"operator": "eq",

"rightOperand": "CSV"

},

{

"leftOperand": "volume",

"operator": "eq",

"rightOperand": {

"@value": "5"

},

"unit": "https://dbpedia.org/page/Gigabyte"

},

{

"leftOperand": "pricePerVolume",

"operator": "eq",

"rightOperand": {

"@value": "10.00",

"@type": "xsd:decimal"

},

"unit": "http://dbpedia.org/resource/Euro"

}

],

[

{

"leftOperand": "type",

"operator": "eq",

"rightOperand": "XML"

},

{

"leftOperand": "volume",

"operator": "eq",

"rightOperand": {

"@value": "3"

},

"unit": "https://dbpedia.org/page/Gigabyte"

},

{

"leftOperand": "pricePerVolume",

"operator": "eq",

"rightOperand": {

"@value": "15.00",

"@type": "xsd:decimal"

},

"unit": "http://dbpedia.org/resource/Euro"

}

]

]

}

]

},

{

"action": "nextPolicy",

"target": "https://agdatahub.provider.demo23.gxfs.fr/policy:1010"

}

],

"constraint": [

{

"leftOperand": "dataVolumePerDay",

"operator": "lteq",

"rightOperand": 5,

"unit": "https://dbpedia.org/page/Gigabyte"

},

{

"leftOperand": "dataVolume",

"operator": "lteq",

"rightOperand": 50,

"unit": "https://dbpedia.org/page/Gigabyte"

},

{

"leftOperand": "odrl:spatial",

"operator": "isAnyOf",

"rightOperand": {

"@list": [

"Europe",

"États-Unis"

]

}

},

{

"leftOperand": "odrl:industry",

"operator": "isAnyOf",

"rightOperand": {

"@list": [

"Technologie"

]

}

},

{

"leftOperand": "odrl:product",

"operator": "isAnyOf",

"rightOperand": {

"@list": [

"Données Financières"

]

}

},

{

"leftOperand": "dateTime",

"operator": "gteq",

"rightOperand": {

"@value": "2025-09-15",

"@type": "xsd:date"

}

},

{

"leftOperand": "dateTime",

"operator": "lt",

"rightOperand": {

"@value": "2025-09-15",

"@type": "xsd:date"

}

},

{

"leftOperand": "fileFormat",

"operator": "in",

"rightOperand": [

"application/pdf",

"image/jpeg"

]

}

]

}

],

"obligation": [

{

"target": "https://minio.demo23.gxfs.fr/dataset/dataset01",

"assigner": "Data Subscriber",

"assignee": "Data Provider Inc.",

"action": "distribute"

}

]

},

"type": "ContractCredential"

},

"validUntil": "2024-12-20T16:11:56.656Z",

"credentialStatus": {

"type": "StatusList2021Entry",

"id": "https://revocation-registry.aster-x.demo23.gxfs.fr/api/v1/revocations/credentials/default-suspension#841",

"statusPurpose": "suspension",

"statusListIndex": "841",

"statusListCredential": "https://revocation-registry.aster-x.demo23.gxfs.fr/api/v1/revocations/credentials/default-suspension"

}

}

}

```
The request's body must include an array of "smartContractsToGenerate" that indicates specifically which smart contracts must be generated and the contract established by the provider and consumer in ODRL format which is the source of the contract terms.


## Routing <a name="routage"></a>


### Smart-contract-manager



{URL} : The url of the Smart-contract-manager



| HTTP Verb | url | input | output | Description |
|-----------|-----------------------------------------------|---------------------|---------------------|--------------------------------------------------------------|
| HTTP POST | https://{URL}/smartContracts | application/json | application/json | Deploy smart contracts from ODRL |
| HTTP POST | https://{URL}/registryGeneration | n/a | application/json | Allows you to connect a new dataspace |
| HTTP POST | https://{URL}/getRegistryData | application/json | application/json | X |
| HTTP GET  | https://{URL}/smartContracts | n/a | application/json | Get data from smart contract in registry |



### Oracle



{URL} : The url of the Oracle



#### Payment



| HTTP Verb | url | input | output | Description |
|-----------|-----------------------------------------------|---------------------|---------------------|--------------------------------------------------------------|
| HTTP POST | https://{URL}/payment | application/json | application/json | Generate payment manually |
| HTTP GET  | https://{URL}/payment | n/a                 | application/json   | Get information about payments |
| HTTP POST | https://{URL}/payment/cron | application/json | application/json | Generate cron payment manually |



#### Billing



| HTTP Verb | url | input | output | Description |
|-----------|-----------------------------------------------|---------------------|---------------------|--------------------------------------------------------------|
| HTTP POST | https://{URL}/bill/ | application/json | application/json | Generate bill manually |
| HTTP GET  | https://{URL}/bill   | n/a               | application/json | Get information about bills |
| HTTP POST | https://{URL}/bill/cron | application/json | application/json | Generate cron payment manually |
| HTTP POST | https://{URL}/bill/period | application/json | application/json | Get bills between two periods |
| HTTP POST | https://{URL}/bill/currency | n/a | application/json | Set currency for an existing smart contract |
| HTTP POST | https://{URL}/bill/pricing | application/json | application/json | Set pricing for an existing smart contract |




## Step to install the project locally <a name="step-to-install-the-project-locally"></a>



```

git clone git@gitlab.com:gaia-x/data-infrastructure-federation-services/deployment-scenario/smart-contract-manager.git

cd ./smart-contract-manager/

npm i

npx hardhat compile

npx hardhat node

```

.env such as

```

EVM_NODE_URL=HARDHAT_URL

PRIVATE_KEY=HARDHAT_PRIVATE_KEY

ORACLE_URL=ORACLE_URL

```

On another terminal instance

```

cd ./smart-contract-manager/

npm run start

```


## Add a plugin <a name="add-plugin"></a>

In order to add a plugin, we first must get an ODRL with this new contract and his conditions
Then add in our ./src/services/smartContractManagerService.js in the jsonpath configuration required to get all the fields we need in the ODRL, we will be using the "location" concept as an exemple :

```
{
	contractName:  'location',
	values: [
		{ key: 'contractDId', path: '$.VC.id' },
		{ key: 'spatialTerm', path: '$..[?(@.duty)].constraint[?(@.leftOperand === "odrl:spatial")]' },
	]
}
```

Next we develop our smart contract in Solidity, this part is totally free but keep in mind that all calcul, data tansformation or any functionnal logic must be developped in the smart contract. On a technical point of view, anything is possible under the limits of Solidity.
On stock notre smart contract ici ./contracts/location.sol, we compile with the command :
```
npx hardhat compile 
```
Next we create our plugin called the same as our contractName, so "location" here

```
const  deploySmartContract  =  require('../../utils/deploySmartContract');
const  plugins  =  require('../../plugins');
const  locationAbiPath  = '../../artifacts/contracts/location.sol/location.json';
const  locationAbi  =  require('../'  +  locationAbiPath).abi

async  function  generateSmartContract(data) {
	try {
	// DATA COME FROM THE ODRL FIELDS PARSED WITH OUR JSONPATH
		const  contract  =  await  deploySmartContract(data.location, locationAbiPath);
		await  plugins.pluginsExecute('registry', 'saveSmartContractData', {
			address:  contract,
			abi:  JSON.stringify(consumptionAbi),
			label:  "location",
			contractDId:  data.contractDId
		}, {})
		return  contract;

	/* Exemple of a chained call to the oracle after the deploiement to deploy a automated task
	await  axios.post(`${process.env.ORACLE_URL}/location/cron`, {...  body,
		contractDId:  contractDId
	});
	*/
	} catch (err) {
		console.log(err);
		throw  new  Error('location SC generation failed')
	}
}
module.exports  = { generateSmartContract };
```

And now we're set for the smart-contract-manager part

Next, it's more of an exemple of how we designed the oracle:

we have 3 potentials parts : functions that englobent our solidity functions, some endpoints and potentially a crontab part

first we create in ./src/services/location.js
```
async function SaveLocation(sc, location) {
    try {
        const infos = await getContractInfosFromDId(sc.contractDId)
        const contract = await getContractConnection(infos)
        const tx = await contract.writeData(location);
        const receipt = await tx.wait();
        console.log('Location data written to the blockchain: ', location);
    } catch (error) {
        throw new Error(`Error writing Location in blockchain: ${error.message}`);
    }
}
```

getContractInfosFromDId() and getContractConnection() instantiate and return a BaseContract object from Ethers.js. We can now call any function found in our Location smart Contract like writeData().

Then we can create routage in ./src/server.js to expose our SaveLocation function

Lastly, to set a periodic task we can create a function that will use the  node-cron module, and the isoToCron function that will translate periodicity data from the ODRL to cron syntaxe :

```
function startCronJob(contractInfos, periodicity, next) {
    const cronExpression = isoToCron(periodicity);
    if (!cronExpression) {
        throw new Error('Invalid periodicity');
    }

    let previous = new Date();
    cron.schedule(cronExpression, () => {
        let now = new Date();
        const location = GenerateLocation();
        SaveLocation(contractInfos, location, previous, now);
        previous = new Date();
    });
    next()
}

```


## Technologies and tools used <a name="technology-and-tool"></a>

- Node.js

- Solidity

- Ethereum

- Hardhat

- Ethers.js

- Paynum



## Authors and acknowledgment <a name="authors-and-acknowledgment"></a>

GXFS-FR



## License <a name="licence"></a>

The Smart Contract Manager is delivered under the terms of the Apache License Version 2.0.